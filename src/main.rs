use cli_input::prelude::*;
use colored::Colorize;
use laminar::{
    Packet,
    Socket,
    SocketEvent
};

const SERVER: &str = "127.0.0.1:12351";

#[derive(Debug, Eq, PartialEq)]
enum Mode {
    Server,
    Client,
}

fn client() -> Result<(), laminar::ErrorKind> {
    let address = "127.0.0.1:12352";
    let mut socket = Socket::bind(address)?;

    println!("Connected on: {}", address.to_string().bright_green().bold());

    let server = SERVER.parse().unwrap();

    println!("Type a message and press enter to send. Send '/quit' to quit.");

    loop {
        let msg = input!("{} ", "Send:".bright_cyan().bold());

        socket.send(Packet::reliable_unordered(server, msg.clone().into_bytes()))?;

        socket.manual_poll(std::time::Instant::now());

        if msg == "/quit" {
            break;
        }

        match socket.recv() {
            Some(SocketEvent::Packet(packet)) => {
                let prefix = {
                    if packet.addr() == server {
                        "Server".to_string()
                    } else {
                        packet.addr().to_string()
                    }
                };

                println!("[{}]: {}", prefix.bright_yellow().bold().underline(), String::from_utf8_lossy(packet.payload()));
            },
            Some(SocketEvent::Timeout(_)) => (),
            _ => println!("Silence..."),
        };
    }

    return Ok(());
}

fn server() -> Result<(), laminar::ErrorKind> {
    let mut socket = Socket::bind(SERVER)?;

    let sender = socket.get_packet_sender();
    let receiver = socket.get_event_receiver();

    let _ = std::thread::spawn(move || socket.start_polling());

    loop {
        if let Ok(event) = receiver.recv() {
            match event {
                SocketEvent::Packet(packet) => {
                    let msg = packet.payload();

                    if msg == b"/quit" {
                        break;
                    }

                    let msg = String::from_utf8_lossy(msg);
                    let ip = packet.addr().ip();

                    println!("[{:?}]: {:?}", ip, msg);

                    sender
                        .send(Packet::reliable_unordered(packet.addr(), "Message recieved!".as_bytes().to_vec()))
                        .unwrap();
                },
                SocketEvent::Timeout(address) => {
                    println!("{} timed out!", address);
                },
                _ => (),
            };
        }
    }

    return Ok(());
}

fn main() {
    let mut mode: Option<Mode> = None;

    loop {
        match input!("[server, client] Please type the mode you want: ").as_str() {
            "server" => { mode = Some(Mode::Server) },
            "client" => { mode = Some(Mode::Client) },
            _ => (),
        };

        if mode != None {
            break;
        }
    }

    let mode = mode.unwrap();

    println!("Selected mode: {:?}", mode);

    match mode {
        Mode::Client => client(),
        Mode::Server => server(),
    }.unwrap();
}
